#! /bin/sh
validInput=false
cd Code/Application
while test $validInput == false
do
    validInput=true
    echo "Which difficulty do you want to face off against? (easy/medium/hard/AI)"
    read choice
    if test $choice == AI
    then
        runipy AI.ipynb
    elif test $choice == easy || test $choice == medium || test $choice == hard
    then
        java Game $choice
    else
        echo "Enter a valid difficulty setting"
        validInput=false
    fi
done