public class Player
{
    private int healthPoints;
    private int manaPoints;
    private int counterPoints;
    private int origHP;
    private int origMP;
    private int origCP;
    public int move;
    public String type;
    public Player()
    {
        healthPoints = origHP = 5;
        manaPoints = origMP = 0;
        counterPoints = origCP = 1;
    }
    public int getHealth()
    {
        return healthPoints;
    }
    public void incHealth(int inc)
    {
        healthPoints += inc;
    }
    public void resHealth()
    {
        healthPoints = origHP;
    }
    public int getMana()
    {
        return manaPoints;
    }
    public void incMana(int inc)
    {
        manaPoints += inc;
    }
    public void resMana()
    {
        manaPoints = origMP;
    }
    public int getCounter()
    {
        return counterPoints;
    }
    public void incCounter(int inc)
    {
        counterPoints += inc;
    }
    public void resCounter()
    {
        counterPoints = origCP;
    }
    public int getMove()
    {
        return move;
    }
    public void setMove(int m)
    {
        move = m;
    }
}

