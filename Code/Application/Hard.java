
import java.util.Scanner;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.io.File;
import java.util.Set;
import java.util.Iterator;

public class Hard{
    HashMap<String, Integer> secondCalculator = new HashMap<String, Integer>();
    
    public Hard(){
        HashMap<String, int[]> firstCalculator = new HashMap<String, int[]>();
        try{
            File file = new File("../Logs/data.txt");
            Scanner reader = new Scanner(file);
            while(reader.hasNext()){
                String status = reader.nextLine();
                int move = Integer.parseInt(status.substring(status.length() - 1));
                status = status.substring(0, status.length() - 3);
                int[] moveSet = firstCalculator.get(status);
                if(moveSet == null){
                    moveSet = new int[7];
                }
                moveSet[move]++;
                firstCalculator.put(status, moveSet);
            }
        }
        catch(FileNotFoundException e){
            System.out.println("Something's wrong bruh");
        }
        
        Set<String> statuses = firstCalculator.keySet();
        for(String status : statuses){
            int[] moveSet = firstCalculator.get(status);
            int move = 0;
            int max = 0;
            for(int i=0;i<moveSet.length;i++){
                if(moveSet[i] >= max){
                    move = i;
                    max = moveSet[i];
                }
            }
            secondCalculator.put(status, move);
        }
    }
    
    public int calculateMove(String status){
        return (secondCalculator.get(status) == null) ? 0 : secondCalculator.get(status);
    }
}