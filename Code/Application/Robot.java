import java.util.Random;
import java.io.FileWriter;
import java.io.IOException;
import java.io.File;
import java.util.Scanner;
import java.io.FileNotFoundException;
import java.util.NoSuchElementException;

public class Robot extends Player
{
    String difficultyLevel;
    Hard calculator;
    
    public Robot(String level)
    {
        type = "robot";
        difficultyLevel = level;
        calculator = new Hard();
    }

    public void setMove(Player other){
        if(difficultyLevel.equals("easy")){
            Random rand = new Random();
            move = 1 + rand.nextInt(7);
        }
        else if(difficultyLevel.equals("medium")){
            Random rand = new Random();
            do{
                move = 1 + rand.nextInt(7);
            }while(((Strike.costs[move-1] > getMana()) || 
            ((move == 3) && (getCounter() == 0))) && 
            ((other.getMana() - getMana()) < 2) || 
            (((move == 2) || (move == 3)) && (other.getMana() == 0)) || 
            ((move == 5) && (getHealth() == 5)));
        }
        else if(difficultyLevel.equals("hard")){
            Random rand = new Random();
            if(rand.nextInt(3) == 0){
                do{
                move = 1 + rand.nextInt(7);
            }while(((Strike.costs[move-1] > getMana()) || 
            ((move == 3) && (getCounter() == 0))) && 
            ((other.getMana() - getMana()) < 2) || 
            (((move == 2) || (move == 3)) && (other.getMana() == 0)) || 
            ((move == 5) && (getHealth() == 5)));
            }
            else{
                move = 1 + calculator.calculateMove(getHealth() + ", " + getMana() + ", " + getCounter() + ", " + 
                                     other.getHealth() + ", " + other.getMana() + ", " + other.getCounter());
            }
        }
        else if(difficultyLevel.equals("AI")){
            try{
                    FileWriter writer = new FileWriter("../Logs/input.txt", false);
                    writer.write(getHealth() + ", " + getMana() + ", " + getCounter() + ", " + 
                                other.getHealth() + ", " + other.getMana() + ", " + other.getCounter());
                    writer.close();
                   }
                catch(IOException e){
                       System.out.println("Can't access input.txt");
                       e.printStackTrace();
                    }
                    
            boolean done = false;        
            File file = new File("../Logs/output.txt");
            long lastModified = file.lastModified();
            while(!done){
                if(lastModified != file.lastModified()){
                    done = true;
                }
            }
            
            done = false;
            while(!done){
                try{
                    Scanner reader = new Scanner(file);
                    move = Integer.parseInt(reader.next()) + 1;
                    reader.close();
                }
                catch(FileNotFoundException e){
                    System.out.println("Can't access output.txt");
                    e.printStackTrace();
                }
                catch(NoSuchElementException e){
                    continue;
                }
                done = true;
            }
        }
        else{
            System.out.println("Difficulty level isn't correctly entered");
        }
    }
}