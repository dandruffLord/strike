import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Game
{
    public static void main(String[] args)
    {
        int timeLimit = 100;
        boolean done = false;
        Scanner kb = new Scanner(System.in);
        try{
                FileWriter writer = new FileWriter("../Logs/status.txt", false);
                writer.write("1");
                writer.close();
        }
        catch(IOException e){
                System.out.println("Can't access status.txt");
        }
        while(!done){
            //Robot one = new Robot("easy");
            //Robot one = new Robot("medium");
            //Robot one = new Robot("hard");
            //Robot one = new Robot("AI");
            //Robot one = new Robot(args[0]);
            Human one = new Human();
            
            //Robot two = new Robot("easy");
            //Robot one = new Robot("medium");
            //Robot two = new Robot("hard");
            //Robot two = new Robot("AI");
            Robot two = new Robot(args[0]);
            //Human two = new Human();
            
            Strike test = new Strike(one, two, timeLimit);
            System.out.println("Do you want to play another game? (y/n)");
            if(!kb.next().equals("y")){
                done = true;
            }
        }
        try{
                FileWriter writer = new FileWriter("../Logs/status.txt", false);
                writer.write("0");
                writer.close();
        }
        catch(IOException e){
                System.out.println("Can't access status.txt");
        }
    }
}