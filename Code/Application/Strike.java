import java.util.Arrays;
import java.util.Scanner;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Strike
{
    public static final String[] moves = {"Charge", "Shield", "Counter", "Punch", "Heal", "Slash", "Blast"};
    public static final String[][] moveList = {{"Charge(1)"},{"Shield(2)","Counter(3)"},{"\u2666","Punch(4)"},{"\u2666\u2666","Heal(5)","Slash(6)"},{"\u2666\u2666\u2666","Blast(7)"}};
    public static final int[] costs = {-1,0,0,1,2,2,3};
    public Strike(Player one, Player two, long limit)
    {
        //Start
        ArrayList<ArrayList<Integer>> movesLog = new ArrayList<ArrayList<Integer>>();
        movesLog.add(new ArrayList<Integer>());
        movesLog.add(new ArrayList<Integer>());
        ArrayList<ArrayList<ArrayList>> statusLog = new ArrayList<ArrayList<ArrayList>>();
        statusLog.add(new ArrayList<ArrayList>());
        statusLog.add(new ArrayList<ArrayList>());
        int oihp = one.getHealth();
        int tihp = two.getHealth();
        int round = 1;
        System.out.println("ROUND " + round);

        while(one.getHealth()>0 && two.getHealth()>0)
        {
            //Roundprep
            if(oihp-one.getHealth()!=0 || tihp-two.getHealth()!=0)
            {
                one.resMana();
                two.resMana();
                one.resCounter();
                two.resCounter();
                System.out.println("ROUND " + ++round);
            }
            statusLog.get(0).add(new ArrayList(Arrays.asList(one.getHealth(), one.getMana(), one.getCounter())));
            statusLog.get(1).add(new ArrayList(Arrays.asList(two.getHealth(), two.getMana(), two.getCounter())));

            //Display
            StringBuilder hud = new StringBuilder();
            hud.append("MoveList: ");
            hud.append(Arrays.deepToString(moveList));
            hud.append(buildHud(one, "One"));
            hud.append(buildHud(two, "Two"));
            System.out.println(hud.toString());
            if(one.type == "human" || two.type == "human"){
                try {
                    Thread.sleep(500);
                }catch (InterruptedException e){
                }
            }
            
            //Input
            oihp = one.getHealth();
            tihp = two.getHealth();
            String inputOne = inputCheck(one, two, "One", limit);
            String inputTwo = inputCheck(two, one, "Two", limit);
            if((inputOne=="") && (inputTwo=="")){
                System.out.println(hud.toString());
                moveCheck(one,two);
                System.out.println("\n\nPlayer One chose to " + moves[one.getMove()-1] + " while Player Two chose to " + moves[two.getMove()-1] + "!\n");
            }
            System.out.print(inputOne + inputTwo);
            if((inputOne!="") && (inputTwo!="")){
                one.incHealth(1);
                two.incHealth(1);
            }
            else if((inputOne!="") || (inputTwo!="")){
                System.out.println(" You lose health!\n");
            }
            movesLog.get(0).add(one.getMove()-1);
            movesLog.get(1).add(two.getMove()-1);
        }
 
        //End
        int winner;
        if(one.getHealth()>0){
                two.incHealth(-two.getHealth());
                System.out.println("Player One is triumphant!");
                winner = 0;
        }
        else{
                one.incHealth(-one.getHealth());
                System.out.println("Player Two is triumphant!");
                winner = 1;
        }
        
        //Logging
        int loser = (winner == 0) ? 1 : 0;
        for(int x=0;x<statusLog.get(0).size();x++){
                statusLog.get(winner).get(x).addAll(statusLog.get(loser).get(x));
                statusLog.get(winner).get(x).add(movesLog.get(winner).get(x));
        }
        try{
                FileWriter writer = new FileWriter("../Logs/data.txt", true);
                writer.write("\n" + statusLog.get(winner).toString().replace("], ","\n").replace("]","").replace("[",""));
                writer.close();
                writer = new FileWriter("../Logs/record.txt", true);
                if(winner == 0){
                    writer.write("O");
                }
                else{
                    writer.write("T");
                }
                writer.close();
        }
        catch(IOException e){
                System.out.println("Can't access data.txt or record.txt");
        }
    }

    public static String buildHud(Player person, String name){
        StringBuilder hud = new StringBuilder();
        hud.append("\nPlayer " + name + ":");
        
        hud.append(" [HP]");
        for(int x=0;x<person.getHealth();x++)
        hud.append("\u2665");
        
        hud.append(" [MP]");
        for(int y=0;y<person.getMana();y++)
        hud.append("\u2666");
        
        hud.append(" [RP]");
        for(int z=0;z<person.getCounter();z++)
        hud.append("\u2617");
        
        return hud.toString();
    }

    public static String inputCheck(Player person, Player other, String name, long limit){
        Scanner kb = new Scanner(System.in);
        long start = System.nanoTime();
        int m = 0;
        System.out.println("Make your move, Player " + name + ", under " + limit + " seconds:");
        if(person.type == "human"){
                m = kb.nextInt();
                person.setMove(m);
        }
        else{
            ((Robot)person).setMove(other);
            m = person.getMove();
        }
        
        person.incHealth(-1);
        if(System.nanoTime()-start>limit*1000000000)
        {
                return "\n\nToo slow, Player " + name + "!";
        }
        if(m>7 || m<1)
        {
                return "\n\nPick a move in the list, Player " + name + "!";
        }
        if(person.getMana()<costs[m-1])
        {
                return "\n\nNot enough mana, Player " + name + "!";
        }
        if(m==3 && person.getCounter()==0)
        {
                return "\n\nYou can't use reflect again this round, Player " + name + "!";
        }
        person.incHealth(1);

        return "";
    }

    public static void moveCheck(Player one, Player two)
    {
        switch(one.getMove()){
            case 1: switch(two.getMove()){
                case 1: one.incMana(1);
                        two.incMana(1);
                        break;
                case 2: one.incMana(1);
                        break;
                case 3: one.incMana(1);
                        two.incCounter(-1);
                        break;
                case 4: one.incHealth(-1);
                        break;
                case 5: two.resHealth();
                        break;
                case 6: one.incHealth(-2);
                        break;
                case 7: one.incHealth(-3);}
                        break;
            case 2: switch(two.getMove()){
                case 1: two.incMana(1);
                        break;
                case 2: break;
                case 3: two.incCounter(-1);
                        break;
                case 4: two.incMana(-1);
                        break;
                case 5: two.resHealth();
                        break;
                case 6: two.incMana(-2);
                        break;
                case 7: one.incHealth(-3);} 
                        break;
            case 3: switch(two.getMove()){
                case 1: one.incCounter(-1);
                        two.incMana(1);
                        break;
                case 2: one.incCounter(-1);
                        break;
                case 3: two.incCounter(-1);
                        one.incCounter(-1);
                        break;
                case 4: two.incHealth(-1);
                        break;
                case 5: two.resHealth();
                        break;
                case 6: one.incHealth(-2);
                        break;
                case 7: one.incHealth(-3);}  
                        break;
            case 4: switch(two.getMove()){
                case 1: two.incHealth(-1);
                        break;
                case 2: one.incMana(-1);
                        break;
                case 3: one.incHealth(-1);
                        break;
                case 4: one.incMana(-1);
                        two.incMana(-1);
                        break;
                case 5: two.incHealth(-2);
                        break;
                case 6: one.incHealth(-1);
                        break;
                case 7: one.incHealth(-3);}    
                        break;
            case 5: switch(two.getMove()){
                case 1: one.resHealth();
                        break;
                case 2: one.resHealth();
                        break;
                case 3: one.resHealth();
                        break;
                case 4: one.incHealth(-2);
                        break;
                case 5: one.resHealth();
                        two.resHealth();
                        break;
                case 6: one.incHealth(-4);
                        break;
                case 7: one.incHealth(-6);}
                        break;
            case 6: switch(two.getMove()){
                case 1: two.incHealth(-2);
                        break;
                case 2: one.incMana(-2);
                        break;
                case 3: two.incHealth(-2);
                        break;
                case 4: two.incHealth(-1);
                        break;
                case 5: two.incHealth(-4);
                        break;
                case 6: one.incMana(-2);
                        two.incMana(-2);
                        break;
                case 7: one.incHealth(-3);}        
                        break;
            case 7: switch(two.getMove()){
                case 1: two.incHealth(-3);
                        break;
                case 2: two.incHealth(-3);
                        break;
                case 3: one.incHealth(-3);
                        break;
                case 4: two.incHealth(-3);
                        break;
                case 5: two.incHealth(-6);
                        break;
                case 6: two.incHealth(-3);
                        break;
                case 7: one.incMana(-3);
                        two.incMana(-3);}            
                   
        }     
    }
   }

